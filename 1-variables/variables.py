
"""

Variables are fundamental building blocks in computer programming.

They can be described as boxes that contain a value stored memory.

These variables can contain pretty much any data type that you can work with in Python.

The four main data types we'll be working for now are:

Integers
Floats
Strings
Booleans

These represent whole numbers, floating (or decimal) point numbers, text, or truthness (True, False)


"""

greeting = "Hello, my name is "
name = "Jimmy"

full_greeting = greeting + name

x = 10
y = 20

z = x + y

print(z)
