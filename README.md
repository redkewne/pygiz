This is a reference repository for ongoing Python courses provided by GIZ through ICTSLab.

# Outline

Intro and Setup

Variables and Data Types

Data Structures

Loops

Functions

Objects and Classes

Testing

VCS

Flask Development

CSS Frameworks

Databases

Deployment

Intro to Machine Learning
