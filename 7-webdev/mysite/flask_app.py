# standard library packages
# installed packages
# your own packages

from flask import Flask, render_template, request, redirect, url_for
from pony.orm import Database, Required, Optional, PrimaryKey, select, db_session
from person import Person


app = Flask(__name__)
db = Database()

class Todo(db.Entity):
    id = PrimaryKey(int, auto=True)
    task_name = Required(str)
    duration = Required(int)
    location = Optional(str)

class Product(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    quantity = Required(int)
    price = Required(int)


db.bind(provider='sqlite', filename='productdb', create_db=True) # username, password, host, database
db.generate_mapping(create_tables=True)


@app.route('/todo2', methods=['GET', 'POST'])
@db_session
def todo2():

    if request.method == 'GET':

        task = Todo.get(task_name='hiking')

        if task:
            task.delete()

        new_things = list(select(t for t in Todo)) # query object

        return render_template('todo.html', TODO=new_things)

    elif request.method == 'POST':

        task_name = request.form.get('task_name')
        duration = request.form.get('duration')
        location = request.form.get('location')

        Todo(task_name=task_name, duration=duration, location=location) # create a new row in our table

        new_things = list(select(t for t in Todo)) # query object

        return render_template('todo.html', TODO=new_things)

@app.route('/', methods=['GET', 'POST'])
def index():

    if request.method == 'GET':

        return render_template('forms.html')

    elif request.method == 'POST':

        name = request.form.get('last_name')
        return f'hello {name}'


@app.route('/table')
def table():

    people = [Person('Ana', 'Tirana', 'Programmer'),
            Person('Deni', 'Sarande', 'Economist'),
            Person('Alda', 'Lushe', 'Nurse'),
            Person('Bledi', 'Tropoja', 'Teacher'),
            Person('Mira', 'Gjiro', 'Economist')]

    return render_template('table.html', PEOPLE=people)

things = [{'task_name':'eat', 'duration': 10, 'location': 'home'},
            {'task_name':'sleep', 'duration': 9000, 'location': 'home'}]


@app.route('/todo', methods=['GET', 'POST'])
def todo():
    # when working with GET, use request.args.get()
    # when working with POST, use request.form()

    if request.method == 'GET':

        return render_template('todo.html', TODO=things)

    elif request.method == 'POST':

        new_dict = {'task_name': request.form.get('task_name', 'DEFAULT_TASK'),
                    'duration': request.form.get('duration', 10),
                    'location': request.form.get('location', 'DEFAULT_LOCATION')}

        things.append(new_dict)


        return render_template('todo.html', TODO=things)


@app.route('/products', methods=['GET', 'POST'])
@db_session
def products():

    if request.method == 'GET':

        searched_term = request.args.get('product', '') # None
        products = select(p for p in Product if searched_term in p.name)

        return render_template('products_table.html', PRODUCTS=products)

    elif request.method == 'POST':
        name = request.form.get('name')
        quantity = request.form.get('quantity')
        price = request.form.get('price')

        Product(name=name, quantity=quantity, price=price)

        return redirect(url_for('products'))

@app.route('/products/delete/<int:id>')
@db_session
def delete_product(id):

    if Product.get(id=id):
        Product[id].delete()

    return redirect(url_for('products'))
