
"""

An object is an instance of a class, that is to say, something that is created
from a class.

A class is a blueprint or prototype for creating objects. In practice,
this means you can specify attributes and methods that all objects from that
class will have in common, even though the values of the attribute of every
object may be different.

Attributes can be declared for the class and/or for the object instances.

A method is a function that belongs to an object. Methods come in different types,
like instance methods, static methods, and class methods.


"""

class Person:

    # Notice the __ in the name of this method. Methods like these are called
    # double-under, or dunder, methods. __init__ is a constructor method.

    def __init__(self, name):
        self.name = name

    def greet(self):
        print(f'Hello, my name is {self.name}')



# After defining the class, we can create an object by passing in arguments for
# every parameter in the constructor, except for self.

x = Person('Drake')

print(x.name)

"""

Another thing we can do with classes is inheritance, where the child class
inherits the methods and attributes of the parent class. Notice the example:

"""

class Human:

    # Here we're defining a class attribute instead of an instance attribute.
    planet = 'Earth'


class Person(Human):

    def __init__(self, name):
        self.name = name

y = Person('Jimmy')

print(y.planet)

"""

Though this course doesn't focus on object-oriented programming as a paradigm,
it's worth noting how polymorphism works. Notice the following example:

"""

class Animal:
    def __init__(self, animal_type):
        self.animal_type = animal_type

class Dog:

    def __init__(self, name):
        self.name = name

    def make_sound(self):
        print(f'{self.name} barks')

class Cat:

    def __init__(self, name):
        self.name = name

    def make_sound(self):
        print(f'{self.name} meows')

animals = [Dog('Sparky'), Cat('Bobby')]

for a in animals:
    a.make_sound()

"""

Todo: super()__init__()

"""